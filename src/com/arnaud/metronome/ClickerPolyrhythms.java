/** \file ClickerPolyrhythms.java */

package com.arnaud.metronome;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;

//import android.util.Log;

/** It's basically an array of Clicker[task] launched at the same time. Every instances of clicker will set isRunning through the class variable so we have a general killswitch (activated multiple times).*/
/** XXX Since this class doesn't inherit from Clicker, it doesn't need to Override the method, they are exclusive to this class (although they share the same api: start() stop() */
public class ClickerPolyrhythms
{
	Clicker[] clicker;
	int n = 0;

	public ClickerPolyrhythms (ContainerPolyrhythms container, double bpm, double bar, Context context)
	{
		n = container.rhythms.length;
		clicker = new Clicker[n];
		//We can't check the specific sound choice, if present, from Clicker(). I want to keep Clicker class clean. So if the user chose specifics sounds, we need to re-initAudio accordingly.
		for (int i=0; i<n; i++) {
			clicker[i] = new Clicker(container.rhythms[i].bpm*bpm/100,container.rhythms[i].bar,context);
			if (container.rhythms[i].ticChoice != -1)
				clicker[i].tic = Signal.getSignal(context,container.rhythms[i].ticChoice,clicker[i].ticFreq,clicker[i].ticLength,clicker[i].sampleRate,container.rhythms[i].bpm*bpm/100);
			if (container.rhythms[i].tocChoice != -1)
				clicker[i].toc = Signal.getSignal(context,container.rhythms[i].tocChoice,clicker[i].tocFreq,clicker[i].tocLength,clicker[i].sampleRate,container.rhythms[i].bpm*bpm/100);
		}
	}

	public void start()
	{//We need to split the procedur so the clickers are all already and start at once
		for (int i=0; i<n; i++) {
			clicker[i].startAudio();
		}
		Clicker.setIsRunning(true);
		if(Build.VERSION.SDK_INT >= 11)
			for (int i=0; i<n; i++)
				clicker[i].clickerTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		else
			for (int i=0; i<n; i++)
				clicker[i].clickerTask.execute();
	}

	public void stop()
	{
		for (int i=0; i<n; i++)
			clicker[i].stop();
	}
}
