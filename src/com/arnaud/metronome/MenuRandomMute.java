/** \file MenuRandomMute.java. RandomMute Menu */

package com.arnaud.metronome;

import android.app.Activity;
import android.os.Bundle;
import android.content.Context;

import android.view.View;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.EditText;

import android.widget.Toast;
import android.app.AlertDialog;
import android.content.DialogInterface;

import android.content.Intent;//For passing data with Metronome

//import android.util.Log;

/** This is very similar to MenuStructures.java.
* It's a dumbed down version.
* Stack several rhythms that will be played simultaneously.
* The number of possible rhythms to be played depends on the number of available cores from the cpu.
*/

public class MenuRandomMute extends Activity
{
	private ContainerRandomMute container;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.menurandommute);
		
		Intent intent = getIntent();
		container = (ContainerRandomMute)intent.getSerializableExtra("containerRandomMute");
		setViewFromContainer(container);
	}
	
	private void setViewFromContainer (ContainerRandomMute container)
	{
		((EditText) findViewById(R.id.menuRandomMuteFreqMeanEditText)).setText(String.valueOf(container.freqMean));
		((EditText) findViewById(R.id.menuRandomMuteFreqDevEditText)).setText(String.valueOf(container.freqDev));
		((EditText) findViewById(R.id.menuRandomMuteLengthMeanEditText)).setText(String.valueOf(container.lengthMean));
		((EditText) findViewById(R.id.menuRandomMuteLengthDevEditText)).setText(String.valueOf(container.lengthDev));
	}

	private boolean setContainerFromView ()
	{
		int freqMean;
		int freqDev;
		int lengthMean;
		int lengthDev;

		container = new ContainerRandomMute();

		if (((EditText) findViewById(R.id.menuRandomMuteFreqMeanEditText)).getText().toString().matches("") || ((EditText) findViewById(R.id.menuRandomMuteFreqDevEditText)).getText().toString().matches("") || ((EditText) findViewById(R.id.menuRandomMuteLengthMeanEditText)).getText().toString().matches("") || ((EditText) findViewById(R.id.menuRandomMuteLengthDevEditText)).getText().toString().matches("")) {
			Toast.makeText(getApplicationContext(),R.string.toastEnterSomeValue,Toast.LENGTH_SHORT).show();
			return false;
		}

		try {
			freqMean = Integer.parseInt(((EditText) findViewById(R.id.menuRandomMuteFreqMeanEditText)).getText().toString());
			freqDev = Integer.parseInt(((EditText) findViewById(R.id.menuRandomMuteFreqDevEditText)).getText().toString());
			lengthMean = Integer.parseInt(((EditText) findViewById(R.id.menuRandomMuteLengthMeanEditText)).getText().toString());
			lengthDev = Integer.parseInt(((EditText) findViewById(R.id.menuRandomMuteLengthDevEditText)).getText().toString());
		} catch (NumberFormatException e) {
			Toast.makeText(getApplicationContext(),R.string.toastIntTooBig,Toast.LENGTH_SHORT).show();
			return false;
		}

		if (freqMean < 0 || freqDev < 0 || lengthMean < 0 || lengthDev < 0) {
			Toast.makeText(getApplicationContext(),R.string.menuRandomMuteToastWarning,Toast.LENGTH_SHORT).show();
			return false;
		}

		container.freqMean = freqMean;
		container.freqDev = freqDev;
		container.lengthMean = lengthMean;
		container.lengthDev = lengthDev;

		return true;
	}

	public void onMenuRandomMuteOk (View v)
	{
		if (setContainerFromView()) {
			Intent intent = new Intent();
			intent.putExtra("containerRandomMute",container);
			setResult(1,intent);
			finish();
		}
	}

	public void onMenuRandomMuteCancel (View v)
	{
		setResult(0,null);
		finish();
	}
	
	public void onMenuRandomMuteSave (View v)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(MenuRandomMute.this);
		builder.setTitle(R.string.dialogSave_title).setMessage(R.string.dialogSave_message);
		builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
			public void onClick (DialogInterface dialog, int id) {
				if (setContainerFromView())
					container.saveToInternal(getApplicationContext());
			}
		});
		builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
			public void onClick (DialogInterface dialog, int id) {
			}
		});

		AlertDialog dialog = builder.create();
		dialog.show();
	}
	
	public void onMenuRandomMuteLoad (View v)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(MenuRandomMute.this);
		builder.setTitle(R.string.dialogLoad_title).setMessage(R.string.dialogLoad_message);
		builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
			public void onClick (DialogInterface dialog, int id) {
				ContainerRandomMute instance = new ContainerRandomMute();
				container = instance.loadFromInternal(getApplicationContext());
				setViewFromContainer(container);
			}
		});
		builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
			public void onClick (DialogInterface dialog, int id) {
			}
		});

		AlertDialog dialog = builder.create();
		dialog.show();
	}
}
