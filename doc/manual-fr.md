# Usage Normal

![](main.png)

Le fonctionnement par défaut de ce métronome est simple. On définit un BPM, une mesure et on appuie sur le bouton *start/stop* vert.

Un menu *réglages* permet de choisir les sons pour le *tic* et le *toc*, ainsi que de garder ou non l'écran allumé.

D'autres modes sont accessibles est permettent d'étendre les fonctionnalités du métronome.

1. BPM: Valeurs décimales possibles. Le minimum possible dépendra de votre téléphone : chez moi c'est ~0.2. Il n'y a pas de limite maximum
2. Mesure: Valeurs décimales possibles. Le premier temps de chaque mesure est un *tic*, le reste *toc*. Ex:
	* 4: mesure à 4 temps (1 *tic* et 3 *tocs*)
	* 1: mesure à 1 temps (que des *tics*)
	* 0: pas de mesure (que des *tocs*)
	* 3.5: 7/8 pensé à la noire (3 noires + 1 croche)
	* 2.66...: 3-3-2 (2 noires pointées + 1 noire)
	* 0.3333....: jouera un débit de triolet de croche
3. Tap In: Un tap-in pour le BPM
4. Mode: Permet de choisir entre le mode Normal ou les modes avancés
5. Menu: Permet d'accéder aux *réglages* de l'application ou de ceux des modes avancés

# Réglages

![](menu_settings.png)

Ce menu permet de choisir les sons séparément pour le *tic* et le *toc* :

* *generated* génère un son pur. La fréquence et durée sont réglables
* *muted* son muet. Très pratique pour les structures (voir plus bas)
* toutes les autres possibilités sont des sons pré-enregistrés

Ici, on a un *stick* pour *tic* et un son généré pour *toc*.

L'option *garder l'écran allumé* offre la possibilité d'agir sur l'application sans devoir déverrouiller le téléphone à chaque fois.

# Modes Avancés

Chaque mode disponible se configure depuis son menu spécifique.
De retour à l'écran principal, il suffira de sélectionner le mode (liste déroulante). Il y a :

* *Sourdine Aléatoire* comme si quelqu'un coupait le volume de manière aléatoire (les mesures continuent de défiler)
* *Polyrythmies* plusieurs métronomes lancés en même temps. La vitesse globale des polyrythmies peut être ajustée depuis l'écran principal via la case BPM qui représente, sous ce mode, un pourcentage
* *Structures* on définit un déroulé où la mesure, le bpm, les sons changent. Idéal pour des équivalences, changements de métriques, ou de bpm. La vitesse globales d'une structure peut être ajustée depuis l'écran principal via la case BPM qui représente, sous ce mode, un pourcentage

![](menu_buttons.png)

* OK/Cancel: Valide/Annule les modifications et retourne à l'écran principal. Ces modifications sont perdues si on quitte le métronome sans sauvegarder
* Sauvegarde/Charge les modifications sur/à partir de téléphone

## Sourdine Aléatoire

![](menu_randommute.png)

Sous ce mode, le métronome peut décider de couper le volume aléatoirement pour une durée elle aussi aléatoire. Les paramètres sont la moyenne et l'incertitude/souplesse par rapport à cette moyenne.

Ici, le son sera coupé en moyenne toutes le 4sec avec une "liberté" de + ou - 2sec. La durée du silence sera 3sec **exactement** car aucun incertitude n'est permise dans ce cas (valeur 0).

## Polyrythmies

![](menu_polyrhythms.png)

Ce menu est assez intuitif, on ajoute plusieurs petits métronomes ayant chacun leurs BPM/mesure. La quantité possible va dépendre du téléphone (le mien n'en fait que 2).

Ici, on veut du 5/4 et du 2/4 avec une équivalence *mesure=mesure*. On définit le BPM de la mesure 5/4 à 150 (arbitraire).

Le battement de la mesure entière est 150/5 = 30. Ce rapport doit être le même pour l'autre équivalence *mesure=mesure*::

* 150/5 = 30
* x/2 = 30 donne x = 2\*30 = 60

Afin de bien distinguer les 2 pulsations, il est possible de définir des sons spécifiques à un des métronome en appuyant sur le bouton *haut-parleur*. Le menu qui s'ouvre permet de choisir entre les réglages généraux ou des sons en particulier.

En utilisant les BPM/mesures à virgules, les sons spécifiques, il est possible de composer des polyrythmies complexes.

De retour sur l'écran principal, la case BPM représente un pourcentage de la vitesse globale des polyrythmies définies.

## Structures

![](menu_structures.png)

Ce mode permet de "programmer" le métronome pour suivre un déroulé avec des équivalences rythmiques ou des changements de BPM ou métrique.
L'idée est de définir les différentes mesures sous forme de *bases* que l'on combine ensuite en *patterns*, on obtient des groupes de mesures. On combine ensuite ces *patterns* pour former des *structures*.

![](menu_structures_expanded.png)

En exemple, *Comrade Conrad - Bill Evans*, 16 mesures en 4/4 puis 16 mesures en 3/4 *noire=noire*. On définit :

* 2 bases à tempo égal. Une en 4, l'autre en 3 :
	* base1 : 120 4
	* base2 : 120 3
* 2 patterns. 1 pour chaque groupe de 16 mesures :
	* pattern1 :
		* base1 x 16
	* pattern2 :
		* base2 x 16
* 1 structure qui contient nos 2 patterns :
	* structure1 :
		* pattern1 x 1
		* pattern2 x 1

Comme pour les polyrythmies, il est possible de choisir des sons spécifiques pour chaque base (bouton *haut-parleur*). Cela peut aider à distinguer les changements.

En se servant des mesures à virgules, en ajustant les bpms entre eux ou en choisissant des sons différents pour chaque *bases*, les structures offrent pas mal de possibilités. L'appli propose 3 exemples de structure qui sont détaillés plus bas.

De retour sur l'écran principal, la case BPM représente un pourcentage de la vitesse globale de notre structure. L'autre case représente le nombre de fois que la structure doit être jouée, 0 pour l'infini.

### Exemple basique

*All The Things You Are* version Gerald Clayton.  
Les 2 premiers A sont en 7 (4+3), le pont en 6 (3+3) et le dernier A en 5 (3+2) sur 12 mesures. Il n'y a pas d'équivalence, c'est noire = noire.

On pourrait simplement utiliser une base en 7, une en 6 et une en 5 mais on va plutôt composer les mesures:

* 3 bases: 4/4, 3/4 et 2/4
	* base1: 240 4
	* base2: 240 3
	* base3: 240 2
* 3 patterns: mesure en 7/4, 6/4 et 5/4
	* pattern1: 4/4 + 3/4
		* base1 x 1
		* base2 x 1
	* pattern2: 3/4 x 2
		* base2 x 2
	* pattern3: 3/4 + 2/4
		* base2 x 1
		* base3 x 1
* 1 structure:
	* structure1: AABA'
		* pattern1 x 8
		* pattern2 x 4
		* pattern3 x 6

### Exemple d'équivalence (preset "All Things")

Il n'y a pas de moyen d'écrire explicitement une équivalence dans les structures. Cependant, on peut penser une équivalence comme un changement de tempo calculé: en appliquant une règle de trois entre l'équivalence et les bpms, on obtient l'effet souhaité.

On reprend l'exemple précédent mais en pensant à la blanche: 4/4 devient 2/2, 3/4 devient 2 noires pointées et 2/4 devient 1/2.

Il n'y a rien de compliqué pour passer à la blanche, on divise le bpm et la mesure par 2.  
Par contre, pour écrire les 2 noires pointées, on va penser "2 blanches mais plus courtes" càd 3/4 de leurs durées autrement dit avec le BPM (on inverse) valant les 4/3 de 120 -> 160.  
*(Si on veut penser en équivalence, la mesure à 2 noires pointées est en fait une mesure à 2/2 précédée d'une équivalence noire pointée devient blanche.)*

En bref, il n'y a  que les bases à changer:

* 3 bases: 2/2, 3/4 et 1/2
	* base1: 120 2 (2 blanches)
	* base2: 160 2 (2 noires pointés. 160 = 120\*4/3)
	* base3: 120 1 (1 blanche)

**Conseils:** Pour faciliter les équivalences, partez d'un BPM facilement divisible par 2 (half-time) et 3 (équivalence triolet ou noire pointée) comme 90, 120, 180. Ça facilitera le calcul.

### Exemples Claves

Il est possible d'abuser du menu Structures pour lui faire jouer des figures rythmiques.

#### Clave "Ewe" (preset "Ewe")

Pour les claves, on va se servir que de "tics" donc de mesures à 1 ou inférieur.

On va la penser (débit 12/8): noire, noire, croche, noire, noire, noire, croche:

* 2 bases: noire et croche
	* base1: 120 1 (noire)
	* base2: 120 0.5 (croche)
* 1 pattern:
	* pattern1:
		* base1 x 2
		* base2 x 1
		* base1 x 3
		* base2 x 1
* 1 structure:
	* structure1:
		* pattern1 x 1

#### Clave 2-3 (preset "2-3")

Dans certaines claves, on a besoin de silences. On va créer une base et règler le son "toc" sur muet.

* 4 bases: noire, croche pointée, croche et demi-soupir (croche muette)
	* base1: 120 1
	* base2: 120 0.75 ou 160 1
	* base3: 120 0.5 ou 240 1
	* base4: 240 0 (+régler le "toc" sur muet)
* 1 pattern:
	* pattern1:
		* base4 x 1
		* base3 x 1
		* base1 x 1
		* base2 x 2
		* base3 x 1
* 1 structure:
	* structure1:
		* pattern1 x 1
