# Normal Use

![](main.png)

The basics of this metronome are quite simple. Define a BPM, a bar then press the *start/stop* green button.

A *settings* menu allows use to choose the sounds for the *tic* and the *toc* and wether or not to keep the screen on.

Other modes are availables that extend the metronome functionalities.

1. BPM: Decimals values allowed. The minimum depends on your phone: mine goes down to ~0.2. There isn't any max limit
2. Bar: Decimals values allowed. Every 1st beat is a "tic", everything else is a "toc". Ex:
	* 4: 4 beats bar
	* 1: 1 beat bar (only "tics")
	* 0: no bars (only "tocs")
	* 3.5: 7/8 groove
	* 2.66...: 3-3-2
	* 0.3333....: will play triolet
3. Tap In: A tap-in for the BPM
4. Mode: Choose between Normal mode or advanced modes
5. Menu: Access the app *settings* or configure advanced modes

# Settings

![](menu_settings.png)

This menu allows you to choose separated sounds for *tic* and *toc* :

* *generated* generate a "pure" sound. Frequency and length are adjustable
* *muted* mute sound. Convenient for the structures (see below)
* every other possibilities are pre-recorded réels sounds

Here, user chose *stick* for *tic* and a generated sound for *toc*.

The *Keep the screen on* option is self descriptive. With that on, the user can act without having to unlock the phone each time.

# Advanced Modes

Each available mode has his own settings menu.
Back to the main screen, you will then select the mode (from the combobox). There are :

* *Random Mute* as if someone was cutting down the volume knob (the beat keeps going though)
* *Polyrhythms* several beats launche simultaneously. The global speed of the polyrhythms can be set from the main screen via the BPM entry, now representing, a percentage
* *Structures* we define a scenario where the bar, the BPM and the sounds change. Perfect for rhythm equivalence, beat changes, tempo changes. The global speed of a structure can be changed from the main screen via the BPM entry, now representing, a percentage

![](menu_buttons.png)

* OK/Cancel: Confirm/Cancel the modifications and goes back to main screen. Those modifications are lost if we leave the application without saving first
* Save/Load the modifications on/from the phone

## Random Mute

![](menu_randommute.png)

Under this mode, the metronome can choose to cut the volume randomly, for a random duration. The parameters are the mean and the uncertainty/flex around this mean.

Here, the sound will be cut every 4sec with a + or - 2sec liberty. The length of the mute will be **exactly** 3sec because we disallow any freedom (value 0).

## Polyrhythms

![](menu_polyrhythms.png)

This menu is inuitive, we pile up several mini-metronomes having separate BPM/mesure. The quantity allowed will depend on your phone (mine is only 2-capable).

Hre, we want 5/4 and 2/4 with a rhythm equivalence *bar=bar*. We define the BPM for the 5/4 bar as 150 (arbitrary).

The whole bar's beat is 150/5 = 30. You want the same rate across the other bar:

* 150/5 = 30
* x/2 = 30 gives x = 2\*30 = 60

In order to be able to distinguish the 2 beats, it is possible to bind specific sounds to a mini-metronome, by pressing the *loud-speaker* button. The menu that opens, offers the choice between generic settings or some specific sounds.

By using decimals BPM/bars, specific sounds, it is possible to compose complex polyrhythms.

Back to the main screen, the BPM entry represents a percentage of the global speed regarding the defined polyrhythms.

## Structures

![](menu_structures.png)

This mode can be used to "program" the metronome into following a scenario with rhythmic equivalences or BPM/Bar changes.
The idea is to first define every different bars as *bases* that will the be combined as *patterns*, we get grouped bars. Then we combine those *patterns* into *structures*.

![](menu_structures_expanded.png)

As an example, *Comrade Conrad - Bill Evans*, 16 bars of 4/4 then 16 bars of 3/4 *4th=4th*. We define:

* 2 bases, same tempo. One in 4, the other in 3 :
	* base1 : 120 4
	* base2 : 120 3
* 2 patterns. 1 for each 1 bars group :
	* pattern1 :
		* base1 x 16
	* pattern2 :
		* base2 x 16
* 1 structure containing both patterns :
	* structure1 :
		* pattern1 x 1
		* pattern2 x 1

Same as the polyrhythms, it is possible to choose specific sounds for each bases (*loud-speaker* button). This may help to better notice changes.

By using decimal bars, adjusting BPMs or by choosing specific sounds for each bases, one can achieve a lot through the structures. The application offers 3 examples of structure that are discussed below.

Back to the main screen, the BPM entry represents a percentage of the global speed of the structure. The other represents the number of repeats for the whole structure, 0 means infinite.

### Basic example

*All The Things You Are* version Gerald Clayton. 
The first 2 A are in 7 (4+3), the bridge is 6 (3+3) and the last A is 5 (3+2) for 12 bars.

Les 2 premiers A sont en 7 (4+3), le pont en 6 (3+3) et le dernier A en 5 (3+2) sur 12 mesures. quarter = quarter.

We could use bars with metric 7, 6 and 5. Instead, let's use composed bars:

* 3 bases: 4/4, 3/4 et 2/4
	* base1: 240 4
	* base2: 240 3
	* base3: 240 2
* 3 patterns: bar's metric 7/4, 6/4 et 5/4
	* pattern1: 4/4 + 3/4
		* base1 x 1
		* base2 x 1
	* pattern2: 3/4 x 2
		* base2 x 2
	* pattern3: 3/4 + 2/4
		* base2 x 1
		* base3 x 1
* 1 structure:
	* structure1: AABA'
		* pattern1 x 8
		* pattern2 x 4
		* pattern3 x 6

### Equivalence example (preset "All Things")

There is no explicit mean to specify an equivalence in the structures menu. However, we can represent an equivalence as a judicious tempo change: with a basic math operation between the wished equivalence and the bpms.

Let's take the previous example but with a different rhythmic perspective. Instead of 4ths, we think halves: 4/4 becomes 2/2, 3/4 becomes 2 pointed 4ths and 2/4 becomes 1/2.

We simply need to halve the bpm and the bar metric.
However, in order to write 2 pointed 4ths, we will use "shorter" halves, like 75% (3/4) of their duration. So we need to increase the bpm by the inverse: 4/3 of 120 -> 160.
*(If we prefer the equivalence approach, let's just say that the 2 pointed 4ths bar is in fact a 2/2 bar preceded by an equivalence pointed 4th = half)*

In short, only the bases need changes:

* 3 bases: 2/2, 3/4 and 1/2
	* base1: 120 2 (2 halves)
	* base2: 160 2 (2 pointed 4ths. 160 = 120*4/3)
	* base3: 120 1 (1 half)

**Advice:** In order to ease equivalence, use a bpm that is divisible by 2 (half-time) and 3 (3-tuplet or pointed 4th) like 90, 120, 180. It eases the maths.

### Claves Examples

It is possible to abuse the Structures menu and make it play rhythms patterns.

#### Clave "Ewe" (preset "Ewe")

For claves, we'll only use "tics. So bar <=1.

Let's think of the clave as a 12/8 pattern: 4th, 4th, 8th, 4th, 4th, 4th, 8th:

* 2 bases: 4th and 8th
	* base1: 120 1 (4th)
	* base2: 120 0.5 (8th)
* 1 pattern:
	* pattern1:
		* base1 x 2
		* base2 x 1
		* base1 x 3
		* base2 x 1
* 1 structure:
	* structure1:
		* pattern1 x 1

#### Clave 2-3 (preset "2-3")

In some cases, we need silences. We'll use a base and set its "toc" sound to "muted".

* 4 bases: 4th, pointed 8th, 8th et half-rest (mute 8th)
	* base1: 120 1
	* base2: 120 0.75 or 160 1
	* base3: 120 0.5 or 240 1
	* base4: 240 0 (bar 0 only plays "tocs", here it's a 8th)
* 1 pattern:
	* pattern1:
		* base4 x 1
		* base3 x 1
		* base1 x 1
		* base2 x 2
		* base3 x 1
* 1 structure:
	* structure1:
		* pattern1 x 1
